import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Principal from './pages/principal/index';



const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Principal} />
        </Switch>
    </BrowserRouter>
);

export default Routes;
