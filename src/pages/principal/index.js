import React, { Component } from "react";
import './index.css';

export default class Index extends Component{
    render(){
        
    return (
        <div>
            <header id="main-header">
                <div className="content">
                    <div className="logo">
                       <p>Logo Aqui</p>
                    </div>
     
                    <nav className="nav">
                        <ul>
                            <li><a href="/">Contratar</a></li>
                            <li><a href="/">Templets</a></li>
                            <li><a href="/">Login</a></li>
                        </ul>
                    </nav>
                </div>
            </header>
            <div className="product-list" >
                <section className="section">
                    <div className="search-box">
                    <button className="button-section">comprar</button>
                    </div>         
                </section>
                <div className="parallax">
                    <div className="content">
                        <h1>Contrate nossos serviços</h1>
                        <p>construção de Site, Aplicativo Mobile e para Desktop</p>
                        <button>Saíba Mais</button>
                    </div>
                </div>
            </div>
            <footer className="footer">
          <div className="content">
              <div className="footer-col">
                  <h1>Sobre a Empresa</h1>
                  <p>Somos uma empresa prestadora de serviço, no qual nos orgulhamos em dizer que somos os melhores.
                        Se vocẽ está querendo um website moderno, leve e refinado não exite em envia-nós uma mensagem.</p>
              </div>
  
  
              <div className="footer-col ">
                  <h1>Social</h1>
                  <ul className="footer-col-social">
                      <li><a href="/"> Twitter</a></li>
                      <li><a href="/"> Facebook</a></li>
                      <li><a href="/"> Instagram</a></li>
                  </ul>
              </div>
  
              <div className="row footer-autorais">
                  Todos direitos reservados © <strong>ZIG Sistema</strong> 
                  <p>desenvolvido por <strong>Leonardo Bussi®</strong></p>
              </div>
        
          </div>
      </footer>
        </div>
    )
    }

}
