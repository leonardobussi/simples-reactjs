import React from 'react';

import Routes from './routes';

import './index.css';

const App = () => (
  <div className="App">
    <Routes />    
  </div>
  
);

export default App;
